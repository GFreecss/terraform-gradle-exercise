terraform {
  backend "s3" {
    bucket = "java-gradle-bucket"
    key = "terraform/state.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "sa-east-1"
}

data "aws_availability_zones" "azs" {}

module "eks-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.0.0"

  name = "${var.cluster_name}-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks
  azs = data.aws_availability_zones.azs.names

  enable_nat_gateway = true # One NAT gateway per subnet
  single_nat_gateway = true # All private subnets will route their internet traffic through this single NAT gateway.
  enable_dns_hostnames = true # The EC2 instances are assigned with a public and private dns names.

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}