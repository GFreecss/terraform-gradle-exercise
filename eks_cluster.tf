module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.15.3"

  cluster_name = "${var.cluster_name}"
  cluster_version = "1.27"

  cluster_endpoint_public_access  = true

  subnet_ids = module.eks-vpc.private_subnets
  vpc_id = module.eks-vpc.vpc_id

  cluster_addons = {
    aws-ebs-csi-driver = {
      most_recent = true
      addon_version = "v1.21.0-eksbuild.1"
      service_account_role_arn = var.aws_ebs_csi_iam_role_arn
    }
  }

  fargate_profiles = {
    default = {
      name = "fargate"
      selectors = [
        {
          namespace = "fargate"
        }
      ]
    }
  }

  eks_managed_node_group_defaults = {
    iam_role_additional_policies = {
      AmazonEBSCSIDriverPolicy = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
    }
  }

  eks_managed_node_groups = {
    green = {
      min_size     = 1
      max_size     = 3
      desired_size = 1

      instance_types = ["t3.small"]
      capacity_type  = "SPOT"
    }
  }

  tags = {
    environment = "development"
    application = "java-gradle"
  }
}