#!/bin/bash

eksctl create iamidentitymapping \
    --cluster $CLUSTER_NAME \
    --region $REGION \
    --arn $USER_ARN \
    --group system:masters \
    --no-duplicate-arns \
    --username $USER_NAME